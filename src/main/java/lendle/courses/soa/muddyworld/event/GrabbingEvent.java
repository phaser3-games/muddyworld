/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.muddyworld.event;

/**
 *
 * @author lendle
 */
public class GrabbingEvent {
    private String folk=null;
    private String mud=null;

    public String getFolk() {
        return folk;
    }

    public void setFolk(String folk) {
        this.folk = folk;
    }

    public String getMud() {
        return mud;
    }

    public void setMud(String mud) {
        this.mud = mud;
    }
    
}
