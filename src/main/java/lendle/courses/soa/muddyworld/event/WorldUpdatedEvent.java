/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.muddyworld.event;

import lendle.courses.soa.muddyworld.model.World;

/**
 *
 * @author lendle
 */
public class WorldUpdatedEvent {
    private long lastUpdate=-1;
    private World partialWorld=null;//only the modified part

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public World getPartialWorld() {
        return partialWorld;
    }

    public void setPartialWorld(World partialWorld) {
        this.partialWorld = partialWorld;
    }
    
    
}
