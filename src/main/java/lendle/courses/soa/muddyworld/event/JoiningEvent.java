/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.muddyworld.event;

/**
 *
 * @author lendle
 */
public class JoiningEvent {
    private String name=null;
    private long tint=-1;

    public long getTint() {
        return tint;
    }

    public void setTint(long tint) {
        this.tint = tint;
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
