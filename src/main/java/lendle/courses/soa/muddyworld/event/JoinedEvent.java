/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.muddyworld.event;

import lendle.courses.soa.muddyworld.model.Player;
import lendle.courses.soa.muddyworld.model.World;

/**
 *
 * @author lendle
 */
public class JoinedEvent {
    private Player generatedPlayer=null;
    private long serverTime=-1;
    private World world=null;

    public Player getGeneratedPlayer() {
        return generatedPlayer;
    }

    public void setGeneratedPlayer(Player generatedPlayer) {
        this.generatedPlayer = generatedPlayer;
    }

    public long getServerTime() {
        return serverTime;
    }

    public void setServerTime(long serverTime) {
        this.serverTime = serverTime;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }
    
    
}
