/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.muddyworld.event;

import java.util.HashMap;
import java.util.Map;
import lendle.courses.soa.muddyworld.model.Mud;
import lendle.courses.soa.muddyworld.model.Player;

/**
 *
 * @author lendle
 */
public class GrabbedEvent {
    private Map<String, String> winners=new HashMap<>();
    
    public GrabbedEvent(Map<Player, Mud> winners){
        for(Player player : winners.keySet()){
            this.winners.put(player.getId(), winners.get(player).getId());
        }
    }

    public Map<String, String> getWinners() {
        return winners;
    }

    
}
