/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.muddyworld.event;

/**
 *
 * @author lendle
 */
public class Events {
    public static final String 
            JOINING="joining",
            JOINED="joined",
            WORLD="world",
            GRABBING="grabbing",
            GRABBED="grabbed";
}
