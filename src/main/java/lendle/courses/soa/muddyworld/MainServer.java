/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.muddyworld;

import com.corundumstudio.socketio.AckMode;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import lendle.courses.soa.muddyworld.event.Events;
import lendle.courses.soa.muddyworld.event.JoinedEvent;
import lendle.courses.soa.muddyworld.event.JoiningEvent;
import lendle.courses.soa.muddyworld.event.WorldUpdatedEvent;
import lendle.courses.soa.muddyworld.model.Mud;
import lendle.courses.soa.muddyworld.model.Player;
import lendle.courses.soa.muddyworld.model.World;

/**
 *
 * @author lendle
 */
public class MainServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        World worldState = new World();
        //generate muds
        for (int i = 0; i < 20; i++) {
            Mud mud = new Mud();
            mud.setId(UUID.randomUUID().toString());
            mud.setLastUpdate(System.currentTimeMillis());
            mud.setX(3200 * Math.random());
            mud.setY(600 * Math.random());
            worldState.getMuds().put(mud.getId(), mud);
        }
        Configuration config = new Configuration();
        config.setAckMode(AckMode.AUTO);
        config.setPort(8000);
        SocketIOServer server = new SocketIOServer(config);
        server.addConnectListener(new ConnectListener() {
            @Override
            public void onConnect(SocketIOClient sioc) {
                System.out.println("connected: " + sioc.getSessionId());
            }
        });
        WorldUpdateThread worldUpdateThread = new WorldUpdateThread(server, worldState);
        worldUpdateThread.start();

        server.addEventListener(Events.JOINING, JoiningEvent.class, new DataListener<JoiningEvent>() {
            @Override
            public void onData(SocketIOClient sioc, JoiningEvent t, AckRequest ar) throws Exception {
                //generate a player
                //sioc.joinRoom("joining");
            }
        });
        Thread joiningThread = new Thread() {
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1000);

                        //if (server.getRoomOperations("joining").getClients().isEmpty() == false) {
                            //System.out.println("boradcast to "+server.getRoomOperations("joining").getClients().size());
                            Player player = new Player();
                            player.setId(UUID.randomUUID().toString());
                            player.setLastUpdate(System.currentTimeMillis());
                            player.setX(Math.random() * 3200);
                            player.setY(Math.random() * 600);
                            player.setUpdated(true);
                            worldState.getPlayers().put(player.getId(), player);
                            worldState.setLastUpdate(System.currentTimeMillis());
                            worldState.setUpdated(true);
                            JoinedEvent joinedEvent = new JoinedEvent();
                            joinedEvent.setGeneratedPlayer(player);
                            joinedEvent.setServerTime(System.currentTimeMillis());
                            joinedEvent.setWorld(worldState);
                            //server.getRoomOperations("joining").sendEvent("joined", joinedEvent);
                            server.getBroadcastOperations().sendEvent("joined", joinedEvent);
//                            for (SocketIOClient client : server.getRoomOperations("joining").getClients()) {
//                                client.leaveRoom("joining");
//                            }
                        //}
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MainServer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        joiningThread.setDaemon(true);
        //joiningThread.start();
        server.addEventListener(Events.WORLD, WorldUpdatedEvent.class, new DataListener<WorldUpdatedEvent>() {
            @Override
            public void onData(SocketIOClient sioc, WorldUpdatedEvent t, AckRequest ar) throws Exception {
                worldState.update(t);
            }
        });

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                server.stop();
            }
        });
        server.start();
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (Throwable e) {
                e.printStackTrace();
                server.stop();
                break;
            }
        }
    }

}
