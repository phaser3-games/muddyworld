/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.muddyworld;

import com.corundumstudio.socketio.SocketIOServer;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import lendle.courses.soa.muddyworld.event.Events;
import lendle.courses.soa.muddyworld.event.GrabbedEvent;
import lendle.courses.soa.muddyworld.model.Mud;
import lendle.courses.soa.muddyworld.model.Player;

/**
 *
 * @author lendle
 */
public class MudGrabbingThread extends Thread {

    private SocketIOServer server = null;
    private Map<Mud, List<Player>> competitors = new HashMap<>();

    public MudGrabbingThread(SocketIOServer server) {
        this.server = server;
        this.setDaemon(true);
    }

    public synchronized void grab(Player player, Mud mud) {
        List<Player> players = competitors.get(mud);
        if (players == null) {
            players = new ArrayList<>();
            competitors.put(mud, players);
            //System.out.println(mud+":"+player);
        }
        if (!players.contains(player)) {
            players.add(player);
        }
    }

    public void run() {
        Gson gson=new Gson();
        while (true) {
            try {
                Thread.sleep(100);
                Map<Player, Mud> winners = new HashMap<>();
                synchronized (this) {
                    if(competitors.isEmpty()){
                        continue;
                    }
                    for (Mud mud : competitors.keySet()) {
                        System.out.println("mud="+mud);
                        List<Player> players = competitors.get(mud);
                       
                        for (Player _player : winners.keySet()) {
                            players.remove(_player);//exclude one who already grabbed muds in this session
                        }
                        if (players.isEmpty()) {
                            continue;
                        }
                        Player winner = players.get(0);
                        winners.put(winner, mud);
                        
                    }
                    competitors.clear();
                }
                if (!winners.isEmpty()) {
                    String json=gson.toJson(new GrabbedEvent(winners));
                    this.server.getBroadcastOperations().sendEvent(Events.GRABBED, json);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(MudGrabbingThread.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
}
