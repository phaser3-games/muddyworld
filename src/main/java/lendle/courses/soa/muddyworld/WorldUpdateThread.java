/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.muddyworld;

import com.corundumstudio.socketio.SocketIOServer;
import com.google.gson.Gson;
import java.util.logging.Level;
import java.util.logging.Logger;
import lendle.courses.soa.muddyworld.event.Events;
import lendle.courses.soa.muddyworld.event.WorldUpdatedEvent;
import lendle.courses.soa.muddyworld.model.World;

/**
 *
 * @author lendle
 */
public class WorldUpdateThread extends Thread {

    private SocketIOServer server = null;
    private World world = null;

    public WorldUpdateThread(SocketIOServer server, World world) {
        this.server = server;
        this.world = world;
        this.setDaemon(true);
    }

    public void run() {
        Gson gson=new Gson();
        while (true) {
            try {
                Thread.sleep(50);
                if (world.isUpdated()) {
                    World partialWorld = world.extractUpdatedPart(true);
                    //System.out.println("updated "+partialWorld.getMuds().size());
                    WorldUpdatedEvent worldUpdatedEvent = new WorldUpdatedEvent();
                    worldUpdatedEvent.setLastUpdate(world.getLastUpdate());
                    worldUpdatedEvent.setPartialWorld(partialWorld);
                    //System.out.println(gson.toJson(worldUpdatedEvent));
                    server.getBroadcastOperations().sendEvent(Events.WORLD, gson.toJson(worldUpdatedEvent));
                    //System.out.println("update sent");
                    world.setUpdated(false);
                }else{
                    //System.out.println("no update found "+System.currentTimeMillis()+", lastUpdate="+world.getLastUpdate()+":"+world);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(WorldUpdateThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
