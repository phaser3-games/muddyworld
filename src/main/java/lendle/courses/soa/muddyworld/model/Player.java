/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.muddyworld.model;

import java.util.Objects;

/**
 *
 * @author lendle
 */
public class Player {
    private String id=null;
    private double x=-1, y=-1;
    private long lastUpdate=-1;
    private boolean updated=false;
    private boolean hasMud=false;
    private boolean left, right, up, down;
    private long tint=-1;

    public long getTint() {
        return tint;
    }

    public void setTint(long tint) {
        this.tint = tint;
    }
    
    

    public boolean isLeft() {
        return left;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public boolean isRight() {
        return right;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public boolean isUp() {
        return up;
    }

    public void setUp(boolean up) {
        this.up = up;
    }

    public boolean isDown() {
        return down;
    }

    public void setDown(boolean down) {
        this.down = down;
    }
    
    

    public boolean isHasMud() {
        return hasMud;
    }

    public void setHasMud(boolean hasMud) {
        this.hasMud = hasMud;
    }
    
    

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }
    
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    
    public void merge(Player player){
        this.setX(player.getX());
        this.setY(player.getY());
        this.setHasMud(player.hasMud);
        this.setLeft(player.isLeft());
        this.setRight(player.isRight());
        this.setUp(player.isUp());
        this.setDown(player.isDown());
        this.setUpdated(true);
        this.setTint(player.getTint());
        this.setLastUpdate(System.currentTimeMillis());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Player other = (Player) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    
}
