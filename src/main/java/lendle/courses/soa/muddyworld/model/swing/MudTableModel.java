/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.muddyworld.model.swing;

import java.util.HashMap;
import java.util.Map;
import javax.swing.table.AbstractTableModel;
import lendle.courses.soa.muddyworld.model.Mud;

/**
 *
 * @author lendle
 */
public class MudTableModel extends AbstractTableModel {

    private Map<String, Mud> muds = null;
    private String[] columns = {"ID", "State", "LastUpdate", "X", "Y", "ControlledBy"};

    public MudTableModel(Map<String, Mud> muds) {
        this.muds = muds;
    }

    @Override
    public int getRowCount() {
        return muds.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String key = (String) muds.keySet().toArray()[rowIndex];
        Mud mud = muds.get(key);
        switch (columnIndex) {
            case 0:
                return mud.getId();
            case 1:
                return mud.getState();
            case 2:
                return mud.getLastUpdate();
            case 3:
                return mud.getX();
            case 4:
                return mud.getY();
            case 5:
                return mud.getControlledBy();
        };
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return Long.class;
            case 3:
                return Double.class;
            case 4:
                return Double.class;
            case 5:
                return String.class;
        };
        return null;
    }

    @Override
    public String getColumnName(int column) {
        return columns[column];
    }

}
