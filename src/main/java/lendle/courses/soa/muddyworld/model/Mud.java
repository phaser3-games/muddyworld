/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.muddyworld.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author lendle
 */
public class Mud {
    private String id=null;
    private double x=-1, y=-1;
    private MudState state=MudState.NORMAL;
    private List<Double> startFlyPosition=new ArrayList<>();
    private long lastUpdate=-1;
    private boolean updated=false;
    private double velocityX=-1, velocityY=-1;
    private String controlledBy=null;

    public String getControlledBy() {
        return controlledBy;
    }

    public void setControlledBy(String controlledBy) {
        this.controlledBy = controlledBy;
    }
    
    

    public double getVelocityX() {
        return velocityX;
    }

    public void setVelocityX(double velocityX) {
        this.velocityX = velocityX;
    }

    public double getVelocityY() {
        return velocityY;
    }

    public void setVelocityY(double velocityY) {
        this.velocityY = velocityY;
    }
    
    

    public List<Double> getStartFlyPosition() {
        return startFlyPosition;
    }

    public void setStartFlyPosition(List<Double> startFlyPosition) {
        this.startFlyPosition = startFlyPosition;
    }

    
    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public MudState getState() {
        return state;
    }

    public void setState(MudState state) {
        this.state = state;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    
    public void merge(Mud mud){
        this.x=mud.getX();
        this.y=mud.getY();
        this.state=mud.getState();
        this.startFlyPosition=mud.getStartFlyPosition();
        this.velocityX=mud.velocityX;
        this.velocityY=mud.velocityY;
        this.controlledBy=mud.controlledBy;
        this.setUpdated(true);
        this.setLastUpdate(System.currentTimeMillis());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mud other = (Mud) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    
}
