/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.muddyworld.model.swing;

import java.util.Map;
import javax.swing.table.AbstractTableModel;
import lendle.courses.soa.muddyworld.model.Player;

/**
 *
 * @author lendle
 */
public class FolkTableModel extends AbstractTableModel {

    private Map<String, Player> players = null;
    private String[] columns = {"ID", "LastUpdate", "X", "Y", "Left", "Right", "Up", "Down"};

    public FolkTableModel(Map<String, Player> players) {
        this.players = players;
    }

    @Override
    public int getRowCount() {
        return players.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String key = (String) players.keySet().toArray()[rowIndex];
        Player player = players.get(key);
        switch (columnIndex) {
            case 0:
                return player.getId();
            case 1:
                return player.getLastUpdate();
            case 2:
                return player.getX();
            case 3:
                return player.getY();
            case 4:
                return player.isLeft();
            case 5:
                return player.isRight();
            case 6:
                return player.isUp();
            case 7:
                return player.isDown();
        };
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
                return Long.class;
            case 2:
                return Double.class;
            case 3:
                return Double.class;
            case 4:
                return Boolean.class;
            case 5:
                return Boolean.class;
            case 6:
                return Boolean.class;
            case 7:
                return Boolean.class;
        };
        return null;
    }

    @Override
    public String getColumnName(int column) {
        return columns[column];
    }

}
