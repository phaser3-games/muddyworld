/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.muddyworld.model;

import java.util.HashMap;
import java.util.Map;
import lendle.courses.soa.muddyworld.event.WorldUpdatedEvent;

/**
 *
 * @author lendle
 */
public class World {

    private Map<String, Mud> muds = new HashMap<>();
    private Map<String, Player> players = new HashMap<>();
    private long lastUpdate = -1;

    private boolean updated = false;

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public Map<String, Player> getPlayers() {
        return players;
    }

    public void setPlayers(Map<String, Player> players) {
        this.players = players;
    }

    public Map<String, Mud> getMuds() {
        return muds;
    }

    public void setMuds(Map<String, Mud> muds) {
        this.muds = muds;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public World extractUpdatedPart(boolean clearUpdatedFlag) {
        World partialWorld = new World();
        for (Map.Entry<String, Player> entry : this.getPlayers().entrySet()) {
            Player _player = entry.getValue();
            if (_player.isUpdated()) {
                if (clearUpdatedFlag) {
                    _player.setUpdated(false);
                }
                partialWorld.getPlayers().put(_player.getId(), _player);
            }
        }
        for (Map.Entry<String, Mud> entry : this.getMuds().entrySet()) {
            Mud _mud = entry.getValue();
            if (_mud.isUpdated()) {
                if (clearUpdatedFlag) {
                    _mud.setUpdated(false);
                }
                partialWorld.getMuds().put(_mud.getId(), _mud);
            }
        }
        return partialWorld;
    }
    
    public void update(WorldUpdatedEvent e){
        //System.out.println("world: "+this+":updating");
        World partialWorld=e.getPartialWorld();
        //System.out.println(partialWorld.getPlayers().size());
        for(Player player : partialWorld.players.values()){
            //System.out.println(player);
            if(this.players.containsKey(player.getId())==false){
                this.players.put(player.getId(), player);
            }
            this.players.get(player.getId()).merge(player);
            //System.out.println("merged "+player);
        }
        for(Mud mud : partialWorld.muds.values()){
            
            if(this.muds.containsKey(mud.getId())==false){
                this.muds.put(mud.getId(), mud);
            }
            this.muds.get(mud.getId()).merge(mud);
        }
        this.setUpdated(true);
        this.setLastUpdate(System.currentTimeMillis());
        //System.out.println("world: "+this+":updated");
    }
}
