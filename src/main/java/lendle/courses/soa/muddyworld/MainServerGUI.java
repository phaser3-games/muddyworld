/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.muddyworld;

import com.corundumstudio.socketio.AckMode;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketConfig;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.google.gson.Gson;
import java.util.List;
import java.util.UUID;
import java.util.Vector;
import javax.swing.SwingUtilities;
import lendle.courses.soa.muddyworld.event.Events;
import lendle.courses.soa.muddyworld.event.GrabbingEvent;
import lendle.courses.soa.muddyworld.event.JoinedEvent;
import lendle.courses.soa.muddyworld.event.JoiningEvent;
import lendle.courses.soa.muddyworld.event.WorldUpdatedEvent;
import lendle.courses.soa.muddyworld.model.Mud;
import lendle.courses.soa.muddyworld.model.Player;
import lendle.courses.soa.muddyworld.model.World;
import lendle.courses.soa.muddyworld.model.swing.FolkTableModel;
import lendle.courses.soa.muddyworld.model.swing.MudTableModel;

/**
 *
 * @author lendle
 */
public class MainServerGUI extends javax.swing.JFrame {

    private SocketIOServer server = null;

    /**
     * Creates new form MainServerGUI
     */
    public MainServerGUI() {
        initComponents();
        Gson gson=new Gson();
        World worldState = new World();
        //generate muds
        for (int i = 0; i < 20; i++) {
            Mud mud = new Mud();
            mud.setId(UUID.randomUUID().toString());
            mud.setLastUpdate(System.currentTimeMillis());
            mud.setX(3200 * Math.random());
            mud.setY(600 * Math.random());
            worldState.getMuds().put(mud.getId(), mud);
        }
        tableMuds.setModel(new MudTableModel(worldState.getMuds()));
        tableFolks.setModel(new FolkTableModel(worldState.getPlayers()));
        Configuration config = new Configuration();
        config.setAckMode(AckMode.MANUAL);
        config.setPort(8000);
        SocketConfig socketConfig = new SocketConfig();
        socketConfig.setReuseAddress(true);
        config.setSocketConfig(socketConfig);
        server = new SocketIOServer(config);
        server.addConnectListener(new ConnectListener() {
            @Override
            public void onConnect(SocketIOClient sioc) {
                System.out.println("connected: " + sioc.getSessionId());
            }
        });
        WorldUpdateThread worldUpdateThread = new WorldUpdateThread(server, worldState);
        worldUpdateThread.start();
        List<JoinedEvent> joinedEventQueue=new Vector<>();
        server.addEventListener(Events.JOINING, JoiningEvent.class, new DataListener<JoiningEvent>() {
            @Override
            public void onData(SocketIOClient sioc, JoiningEvent t, AckRequest ar) throws Exception {
                //generate a player
                Player player = new Player();
                player.setId(UUID.randomUUID().toString());
                player.setLastUpdate(System.currentTimeMillis());
                player.setX(Math.random() * 500);
                player.setY(Math.random() * 600);
                player.setUpdated(true);
                player.setTint(t.getTint());
                worldState.getPlayers().put(player.getId(), player);
                worldState.setLastUpdate(System.currentTimeMillis());
                worldState.setUpdated(true);
                JoinedEvent joinedEvent = new JoinedEvent();
                joinedEvent.setGeneratedPlayer(player);
                joinedEvent.setServerTime(System.currentTimeMillis());
                joinedEvent.setWorld(worldState);
                joinedEventQueue.add(joinedEvent);
                sioc.sendEvent("joined", gson.toJson(joinedEvent));
            }
        });

        server.addEventListener("world", WorldUpdatedEvent.class, new DataListener<WorldUpdatedEvent>() {
            @Override
            public void onData(SocketIOClient sioc, WorldUpdatedEvent t, AckRequest ar) throws Exception {
                //System.out.println(gson.toJson(t.getPartialWorld().getPlayers()));
                worldState.update(t);
                SwingUtilities.invokeLater(new Runnable(){
                    @Override
                    public void run() {
                        tableMuds.updateUI();
                        tableFolks.updateUI();
                    }
                });
            }
        });
        
        MudGrabbingThread mudGrabbingThread=new MudGrabbingThread(server);
        mudGrabbingThread.start();
        
        server.addEventListener("grabbing", GrabbingEvent.class, new DataListener<GrabbingEvent>(){
            @Override
            public void onData(SocketIOClient sioc, GrabbingEvent t, AckRequest ar) throws Exception {
                Mud mudInstance=worldState.getMuds().get(t.getMud());
                if(mudInstance==null){
                    return;
                }
                Player playerInstance=worldState.getPlayers().get(t.getFolk());
                mudGrabbingThread.grab(playerInstance, mudInstance);
            }
            
        });
        

        server.start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
     
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableMuds = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableFolks = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel1.setText("Server is running......");

        tableMuds.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tableMuds);

        jTabbedPane1.addTab("Muds", jScrollPane1);

        tableFolks.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tableFolks);

        jTabbedPane1.addTab("Folks", jScrollPane2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        this.server.stop();
        System.exit(0);
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainServerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainServerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainServerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainServerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainServerGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable tableFolks;
    private javax.swing.JTable tableMuds;
    // End of variables declaration//GEN-END:variables
}
