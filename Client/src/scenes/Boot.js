import Phaser from 'phaser'
import rocket from 'images/pixel_ship_red_small_2.png'
import laser from 'images/pixel_laser_small_blue.png'
import asteroid from 'images/asteroid_grey_tiny.png'
import gold from 'images/asteroid_tiny.png'
import rocket_collide from 'images/rocket_collide.png'
import Townsfolk_M_Idle_1 from 'images/Townsfolk_M_Idle_1.png'
import Folk_M_Idle_2 from 'images/Folk_M_Idle_2.png'
import bg from 'images/y1ahMp.jpg'
import mud from 'images/Mud_Particle.png'

class Boot extends Phaser.Scene {
  constructor () {
    super({ key: 'Boot' })
  }

  preload () {
    this.load.image('rocket', rocket)
    this.load.image('laser', laser)
    this.load.image('asteroid', asteroid)
    this.load.image('gold', gold)
    this.load.spritesheet('rocket_collide_anim', rocket_collide, { frameWidth: 70, frameHeight: 50 });
    this.load.image('bg', bg);
    this.load.image('mud', mud);
    this.load.spritesheet("folk", Townsfolk_M_Idle_1, {
      frameWidth: 80,
      frameHeight: 80
    });
    this.load.spritesheet("folk2", Folk_M_Idle_2, {
      frameWidth: 80,
      frameHeight: 80
    });
  }

  create () {
    this.scene.start('Game')
  }
}

export {Boot};