import Phaser from 'phaser'
import { Folk } from '../sprites/Folk.js'
import { Mud } from '../sprites/Mud.js'
import { FolkController } from '../sprites/FolkController.js'
import { MudController } from '../sprites/MudController.js'
import io from 'socket.io-client'

class Game extends Phaser.Scene {
  constructor() {
    super({ key: 'Game' })
    this.lastUpdate = Date.now();
  }

  create() {
    this.flag = Math.floor(Math.random() * 16777215);
    document.getElementById("flag").style.backgroundColor = "#" + this.flag.toString(16);
    let self = this;
    this.socket = io("http://localhost:8000");
    this.add.tileSprite(1600, 300, 3200, 600, "bg");
    this.cameras.main.setBounds(0, 0, 3200, 600);
    this.graphics = this.add.graphics();
    this.muds = this.physics.add.group();
    this.flyingMuds = this.physics.add.group();
    this.anims.create({
      key: "folk",
      frames: this.anims.generateFrameNumbers("folk", {
        start: 0,
        end: 0
      }),
      frameRate: 20,
      repeat: -1
    });
    this.anims.create({
      key: "folk_mud",
      frames: this.anims.generateFrameNumbers("folk2", {
        start: 0,
        end: 0
      }),
      frameRate: 20,
      repeat: -1
    });
    self.folkController = new FolkController(self);
    this.socket.on('world', function (e) {
      if (!self.folkController || !self.folkController.mainFolk) {
        return;
      }
      e = JSON.parse(e);
      for (let key in e.partialWorld.players) {
        let player = self.folkController.allFolks[key];
        if (player) {
          //update the player
          if (player.id == self.folkController.mainFolk.id) {
            continue;
          }
          player.updateState(e.partialWorld.players[key]);
        } else {
          //create the player
          let remotePlayer = e.partialWorld.players[key];
          player = self.folkController.createFolk(10, 10, true, remotePlayer.tint, key);
          self.physics.add.overlap(player, self.mudController.mudGroup, function (player, mud) {
            if(mud.state=="FLYING" && mud.controlledBy==self.folkController.mainFolk.id){
              let now=Date.now();
              if((now-player.lastShot)>1000){
                player.lastShot=now;
                self.folkController.mainFolk.score++;
              }
            }
          });
        }
      }

      for (let key in e.partialWorld.muds) {
        let mud = self.mudController.allMuds[key];
        mud.updateState(e.partialWorld.muds[key]);
      }
    });
    this.socket.on('joined', function (e) {
      e = JSON.parse(e);
      console.log(e);
      if (!self.folkController) {
        return;
      }
      self.folk = self.folkController.createFolk(e.generatedPlayer.x, e.generatedPlayer.y, false, self.flag, e.generatedPlayer.id);
      self.folkController.mainFolk = self.folk;
      self.mudController = new MudController(self, self.folk, self.folkController);
      let camera = self.cameras.main;
      camera.startFollow(self.folk);
      for (let mudKey in e.world.muds) {
        let mud = e.world.muds[mudKey];
        self.mudController.createMud(mudKey, mud.x, mud.y, true);
      }
      self.mudController.mudGroup.children.iterate(function(o){
        o.setBounce(0.5);
      });
      // self.physics.add.overlap(self.folkController.mainFolk, player, function(){
      //   self.folkController.mainFolk.score++;
      // });
    });

    setTimeout(function () {
      self.socket.emit('joining', {
        "name": "random" + Date.now(),
        "tint": self.flag
      });
    }, 1000);
    this.scoreText = this.add.text(700, 10, "Score: 0");
    this.scoreText.setScrollFactor(0);
  }

  update() {
    if (this.folkController && this.mudController) {
      this.scoreText.setText("Score: " + this.folkController.mainFolk.score);
      this.folkController.update();
      this.mudController.update();

      let now = Date.now();
      if ((now - this.lastUpdate) > 50) {
        this.lastUpdate = now;
        let updatedFolks = this.folkController.collectUpdates();
        let updatedFolksMap = {};
        for (let folk of updatedFolks) {
          updatedFolksMap[folk.id] = folk;
        }
        let updatedMuds = this.mudController.collectUpdates();
        let updatedMudsMap = {};
        for (let mud of updatedMuds) {
          updatedMudsMap[mud.id] = mud;
        }
        this.socket.emit('world', {
          "partialWorld": {
            "players": updatedFolksMap,
            "muds": updatedMudsMap
          }
        });
      }
    }
  }
}

export { Game };