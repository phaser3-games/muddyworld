import {BaseSprite} from './BaseSprite.js'

class Folk extends BaseSprite {
  constructor(scene, id, x, y, tint) {
    super(scene, id, x, y, 'folk');
    
    this.left=this.right=this.up=this.down=false;
    this.mud=null;
    this.updated=false;
    this.hasMud=false;
    this.lastUpdate=-1;
    this.flag=tint;
    this.setTint(this.flag);
    this.score=0;
    this.setBounce(0.5);
    this.lastShot=-1;
  }


  goLeft() {
    this.left = true;
    this.right=false;
  }
  goRight() {
    this.right = true;
    this.left=false;
  }

  goUp(){
    this.up=true;
    this.down=false;
  }

  goDown(){
    this.down=true;
    this.up=false;
  }

  idle() {
    //this.left = this.right = this.up=this.down=false;
  }

  updateState(state){
    this.lastUpdate=Date.now();
    this.x=state.x;
    this.y=state.y;
    this.left=state.left;
    this.right=state.right;
    this.up=state.up;
    this.down=state.down;
    this.hasMud=state.hasMud;
    if(this.left){
      this.flipX=true;
    }else{
      this.flipX=false;
    }
    if(this.hasMud){
      this.anims.play("folk_mud");
    }else{
      this.anims.play("folk");
    }
  }

  update() {
    this.lastUpdate=Date.now();
    this.setVelocity(0);
    if(this.left){
      this.flipX=true;
      this.setVelocityX(-50);
      this.updated=true;
    }else if(this.right){
      this.flipX=false;
      this.setVelocityX(50);
      this.updated=true;
    }
    if(this.up){
      this.setVelocityY(-50);
      this.updated=true;
    }else if(this.down){
      this.setVelocityY(50);
      this.updated=true;
    }
    if(this.hasMud){
      this.anims.play("folk_mud");
    }else{
      this.anims.play("folk");
    }
    if(this.updated){
      if(this.mud!=null){
        this.mud.x=this.x;
        this.mud.y=this.y;
        this.mud.updated=true;
      }
    }
    
  }
}
export { Folk };