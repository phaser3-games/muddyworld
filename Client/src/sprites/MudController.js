import { Mud } from './Mud.js'

class MudController {
    constructor(scene, mainFolk, folkController) {
        this.scene = scene;
        this.folkController=folkController;
        this.groundGroup = scene.physics.add.group();
        this.flyingGroup = scene.physics.add.group();
        this.mudGroup = scene.physics.add.group();
        this.mainFolk = mainFolk;
        this.allMuds={};
        let self=this;
        this.grabbing=false;
        scene.physics.add.overlap(this.mainFolk, this.mudGroup, function (o1, o2) {
            if (o1.mud == null && !self.grabbing && o2.state=="NORMAL") {
                //o1.mud = o2;
                //o2.disableBody(true, true);
                //self.groundGroup.remove(o2, true, true);
                //o2.alpha=0;
                self.grabbing=true;
                scene.socket.emit('grabbing', {
                    folk: o1.id,
                    mud: o2.id
                });
            }
        });
        scene.socket.on('grabbed', function(e){
            e=JSON.parse(e);
            let winners=e.winners;
            for(let folkPeer in winners){
                let mudPeer=winners[folkPeer];
                //console.log(folkPeer);
                let folk=self.folkController.allFolks[folkPeer];
                let mud=self.allMuds[mudPeer];
                mud.controlledBy=folk.id;
                mud.state="HELD";
                mud.updated=true;
                mud.lastUpdate=Date.now();
                mud.alpha=0;
                folk.mud=mud;
                folk.hasMud=true;
                //mud.disableBody(true, true);
                //self.groundGroup.remove(mud, true, true);
            }
            self.grabbing=false;
        });
        // for (let i = 0; i < 10; i++) {
        //     this.createMud(Math.random() * 3200, Math.random() * 600, false);
        // }
    }

    createMud(id, x, y, remote, group=this.mudGroup) {
        //let id = "" + Date.now() + Math.random();//generate random id
        let mud = new Mud(this.scene, id, x, y);
        mud.setVelocity(0,0);
        mud.setRemoteControlled(remote);
        group.add(mud);
        this.allMuds[id]=mud;
        return mud;
    }

    getGroundGroup() {
        return this.groundGroup;
    }

    getFlyingGroup() {
        return this.flyingGroup;
    }
    calculateDegreesFromRadian(radian) {
        return radian * 180 / Math.PI;
    }
    calculateAngleInDegrees(x, y, x2, y2) {
        return this.calculateDegreesFromRadian(Phaser.Math.Angle.Between(x, y, x2, y2));
    }

    /**
     * return list of updates of muds
     */
    collectUpdates(){
        let array=[];
        for(let key in this.allMuds){
            let mud=this.allMuds[key];
            if(mud.updated){
                mud.updated=false;
                array.push({
                    id: mud.id,
                    x: mud.x,
                    y: mud.y,
                    velocityX: (mud.body.velocity.x)?mud.body.velocity.x:0,
                    velocityY: (mud.body.velocity.y)?mud.body.velocity.y:0,
                    state: mud.state,
                    controlledBy: mud.controlledBy,
                    startFlyPosition: mud.startFlyPosition,
                    lastUpdate: mud.lastUpdate
                });  
            }
        }
        return array;
    }

    update() {
        let scene = this.scene;
        let pointer = scene.input.activePointer;
        if (this.mainFolk.mud != null && pointer.primaryDown) {
            let v = (scene.physics.velocityFromAngle(
                this.calculateAngleInDegrees(
                    this.mainFolk.x,
                    this.mainFolk.y, pointer.x + scene.cameras.main.scrollX,
                    pointer.y + scene.cameras.main.scrollY), 500));
            let mud=this.mainFolk.mud;
            mud.alpha=1;
            //let mud = this.createMud(this.mainFolk.x, this.mainFolk.y, false, this.flyingGroup);
            mud.setVelocityX(v.x);
            mud.setVelocityY(v.y);
            mud.setVelocity(v.x, v.y);
            mud.state = "FLYING";
            mud.startFlyPosition = [this.mainFolk.x, this.mainFolk.y];
            mud.updated=true;
            mud.lastUpdate=Date.now();
            //console.log(this.allMuds[mud.id]);
            this.mainFolk.mud = null;
            this.mainFolk.hasMud = false;
            this.mainFolk.updated=true;
        }
        for(let key in this.allMuds){
            this.allMuds[key].update();
        }
        // let tobeRemoved = [];
        // let self=this;
        // this.flyingGroup.children.iterate(function (mud) {
        //     mud.update();
        //     if (mud.body.velocity.x == 0 && mud.body.velocity.y == 0) {
        //         tobeRemoved.push(mud);
        //         self.createMud(mud.x, mud.y, false);
        //     }
        // });
        // for (let m of tobeRemoved) {
        //     self.flyingGroup.remove(m, true, true);
        // }
    }
}

export { MudController }