import Phaser from 'phaser'

class Laser extends Phaser.Physics.Arcade.Sprite {
  constructor (scene, x, y, velocity) {
    super(scene, x, y, 'laser');
    scene.add.existing(this);
    scene.physics.world.enable(this);
    this.body.setGravity(0);
    this.velocity=velocity;
    
  }

  update () {
    this.setVelocity(this.velocity.x, this.velocity.y);
  }
}
export {Laser};