import Phaser from 'phaser'
import { Laser } from './Laser.js'

class Rocket extends Phaser.Physics.Arcade.Sprite {
  constructor(scene, id, x, y, flag) {
    super(scene, x, y, 'rocket');
    scene.add.existing(this);
    scene.physics.world.enable(this);
    this.body.setGravity(0);
    this.body.setCollideWorldBounds(true);

    this.id=id;
    this.rotateRight = false;
    this.rotateLeft = false;
    this.speed = 0;
    this.lastShoot = -1;
    this.flag=flag;
  }

  shoot() {
    let current = Date.now();
    if ((current - this.lastShoot) > 1000) {
      let v = (this.scene.physics.velocityFromRotation((this.angle - 90) % 360 * Math.PI / 180, 150));
      let laser = new Laser(this.scene, this.x, this.y, v);
      laser.angle = this.angle;
      this.lastShoot = current;
      return laser;
    } else {
      return null;
    }
  }

  goLeft() {
    this.rotateLeft = true;
  }
  goRight() {
    this.rotateRight = true;
  }
  idle() {
    this.rotateLeft = this.rotateRight = false;
  }
  doAccelerate() {
    this.speed = Math.min(this.speed + 1, 200);
  }
  doDeaccelerate() {
    this.speed = Math.max(this.speed - 1, 0);
  }

  update(state) {
    //this.setAlpha(0.5);
    this.setTint(this.flag);
    if (!state) {
      this.setVelocity(0);
      if (this.rotateLeft) {
        this.angle = (this.angle - 1) % 360;
      } else if (this.rotateRight) {
        this.angle = (this.angle + 1) % 360;
      }
      let v = (this.scene.physics.velocityFromRotation((this.angle - 90) % 360 * Math.PI / 180, this.speed));
      this.setVelocity(v.x, v.y);
    }else{
      this.x=state.x;
      this.y=state.y;
      this.angle=state.angle;
    }
  }
}
export { Rocket };