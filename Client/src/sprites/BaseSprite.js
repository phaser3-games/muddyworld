import Phaser from 'phaser'

class BaseSprite extends Phaser.Physics.Arcade.Sprite {
  constructor(scene, id, x, y, texture) {
    super(scene, x, y, texture);
    scene.add.existing(this);
    scene.physics.world.enable(this);
    this.body.setGravity(0);
    this.id=id;
    this.remoteControlled=false;
    this.lastUpdate=-1;
  }

  setRemoteControlled(v){
    this.remoteControlled=v;
  }

  isRemoteControlled(){
    return this.remoteControlled;
  }

  updateState(state){

  }

  update() {
  }
}
export { BaseSprite };