import { Folk } from './Folk.js'

class FolkController {
    constructor(scene) {
        this.scene = scene;
        this.group = scene.physics.add.group();
        this.allFolks={};
        //this.mainFolk = this.createFolk(300, 300, false);
        this.mainFolk = null;
    }

    createFolk(x, y, remote, flag, _id) {
        let id = (_id)?_id:"" + Date.now() + Math.random();//generate random id
        let folk = new Folk(this.scene, id, x, y, flag);
        folk.setRemoteControlled(remote);
        this.group.add(folk);
        this.allFolks[id]=folk;
        return folk;
    }

    getMainFolk() {
        return this.mainFolk;
    }

    getGroup() {
        return this.group;
    }

    update() {
        let scene=this.scene;
        //this.mainFolk.idle();
        let pointer = scene.input.activePointer;

        if (pointer.x + scene.cameras.main.scrollX > this.mainFolk.x) {
            this.mainFolk.goRight();
        } else if (pointer.x + scene.cameras.main.scrollX < this.mainFolk.x) {
            this.mainFolk.goLeft();
        }

        if (pointer.y + scene.cameras.main.scrollY > this.mainFolk.y) {
            this.mainFolk.goDown();
        } else if (pointer.y + scene.cameras.main.scrollY < this.mainFolk.y) {
            this.mainFolk.goUp();
        }
        //console.log(this.mainFolk.left);
        this.mainFolk.update();
        let now=Date.now();
        for(let key in this.allFolks){
            let folk=this.allFolks[key];
            if(folk.lastUpdate!=-1 && (now-folk.lastUpdate)>60000){
                folk.disableBody(true, true);
                if(folk.mud!=null){
                    folk.mud.state="NORMAL";
                    folk.mud.updated=true;
                }
                delete this.allFolks[key];
            }
        }
    }
    /**
     * return list of updates of folks
     */
    collectUpdates(){
        let array=[];
        for(let key in this.allFolks){
            let folk=this.allFolks[key];
            //console.log("update: "+folk.right+":"+this.mainFolk.left+":"+this.mainFolk.body.velocity.x+":"+(folk==this.mainFolk));
            if(folk.updated){
                folk.updated=false;
                array.push({
                    id: folk.id,
                    x: folk.x,
                    y: folk.y,
                    left: folk.left,
                    right: folk.right,
                    up: folk.up,
                    down: folk.down,
                    hasMud: folk.hasMud,
                    tint: folk.flag,
                    score: folk.score
                });  
            }
        }
        return array;
    }
}

export { FolkController }