import {BaseSprite} from './BaseSprite.js'

class Mud extends BaseSprite {
  constructor(scene, id, x, y) {
    super(scene, id, x, y, 'mud');
    
    this.startFlyPosition=null;
    this.state="NORMAL";//NORMAL, HELD, FLYING
    this.controlledBy=null;//controlled by which folk
    this.updated=false;
  }

  updateState(state){
    if(this.lastUpdate>state.lastUpdate){
      return;
    }
    if(this.state=="FLYING" && state.state=="FLYING"){
      this.controlledBy=state.controlledBy;
      return;
    }
    this.x=state.x;
    this.y=state.y;
    //console.log(state.velocityX+":"+state.state);
    this.setVelocity(state.velocityX, state.velocityY);
    this.state=state.state;
    this.controlledBy=state.controlledBy;
    this.startFlyPosition=state.startFlyPosition;
    if(this.state=="HELD"){
      this.alpha=0;
    }else{
      this.alpha=1;
    }
  }

  update(){
    if(this.state=="FLYING"){
      this.updated=true;
      this.alpha=1;
      if(this.y<=0 || this.y>=600 || this.x<=0 || this.x>=3200 || 
        Phaser.Math.Distance.Between(this.startFlyPosition[0], this.startFlyPosition[1], this.x, this.y)>=300){
        this.setVelocity(0, 0);
        this.state="NORMAL";
      }
    }
  }
}
export { Mud };